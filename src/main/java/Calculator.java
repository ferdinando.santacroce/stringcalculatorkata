public class Calculator {
    public int add(String numbers) {
        if ("".equals(numbers)) {
            return 0;
        }
        int sum = 0;
        String arrNumbers[] = numbers.split(",|\n");
        for(int i=0; i<arrNumbers.length; i++){
            int number = Integer.valueOf(arrNumbers[i]);
            sum = sum + number;
        }
        return sum;

    }
}
