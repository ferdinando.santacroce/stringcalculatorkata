import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @Test
    public void stringBlankShouldReturnZero() {
        //Arrange
        Calculator calculator = new Calculator();

        //Act
        int actualResult = calculator.add("");

        //Assert
        int expectedResult = 0;
        assertEquals(expectedResult, actualResult);

    }

    @Test
    public void string99ShouldReturnNumber99() {
        //Arrange
        Calculator calculator = new Calculator();

        //Act
        int actualResult = calculator.add("99");

        //Assert
        int expectedResult = 99;
        assertEquals(expectedResult, actualResult);

    }

    @Test
    public void stringNumber3ShouldReturnNumber3() {
        //Arrange
        Calculator calculator = new Calculator();

        //Act
        int actualResult = calculator.add("3");

        //Assert
        int expectedResult = 3;
        assertEquals(expectedResult, actualResult);

    }

    @Test
    public void string1Comma2ShouldReturn3() {
        //Arrange
        Calculator calculator = new Calculator();

        //Act
        int actualResult = calculator.add("1,2");

        //Assert
        int expectedResult = 3;
        assertEquals(expectedResult, actualResult);

    }

    @Test
    public void string1CommaBlankShouldReturn1() {
        //Arrange
        Calculator calculator = new Calculator();

        //Act
        int actualResult = calculator.add("1,");

        //Assert
        int expectedResult = 1;
        assertEquals(expectedResult, actualResult);

    }

    @Test
    public void string123ShouldReturn6() {
        //Arrange
        Calculator calculator = new Calculator();

        //Act
        int actualResult = calculator.add("1,2,3");

        //Assert
        int expectedResult = 6;
        assertEquals(expectedResult, actualResult);

    }
    @Test
    public void string1NewLine2ShouldReturnNumber3() {
        //Arrange
        Calculator calculator = new Calculator();

        //Act
        int actualResult = calculator.add("1\n2");

        //Assert
        int expectedResult = 3;
        assertEquals(expectedResult, actualResult);

    }
}
